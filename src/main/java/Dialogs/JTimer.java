package Dialogs;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.Call;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.Timer;

public class JTimer extends JFrame {
	
	private JPanel contentPane;
	private JLabel label;
	private int cntr;
	private static JTimer frame;
	static Timer t;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {			
					frame = new JTimer();	
					t.start();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JTimer() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 340, 235);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		label = new JLabel("5");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.PLAIN, 60));
		contentPane.add(label, BorderLayout.CENTER);
		
		JLabel lblRecordingStartsIn = new JLabel("Recording Starts in:");
		lblRecordingStartsIn.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRecordingStartsIn.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblRecordingStartsIn, BorderLayout.NORTH);
		
		cntr=4;
		t = new Timer(1000 * 1, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	if(cntr==0){
            		frame.setVisible(false);
            		Call.workspace.startScreenRecording();
            		frame.dispose();
            		
            	}
            	label.setText(Integer.toString(cntr));
            	cntr--;
            }
		});
	}

}
